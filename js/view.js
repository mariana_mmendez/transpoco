function BxWallView(oOptions) {
    this._sActionsUrl = oOptions.sActionUrl;
    this._sObjName = oOptions.sObjName == undefined ? 'oWallView' : oOptions.sObjName;
    this._iOwnerId = oOptions.iOwnerId == undefined ? 0 : parseInt(oOptions.iOwnerId);
    this._iGlobAllowHtml = 0;
    this._sAnimationEffect = oOptions.sAnimationEffect == undefined ? 'slide' : oOptions.sAnimationEffect;
    this._iAnimationSpeed = oOptions.iAnimationSpeed == undefined ? 'slow' : oOptions.iAnimationSpeed;
}
BxWallView.prototype.deletePost = function(iId) {
    var $this = this;
    var oData = this._getDefaultData();
    oData['WallEventId'] = iId;
    
    $this._loading(true);

    $.post(
        this._sActionsUrl + 'delete/',
        oData,
        function(oData) {
            $this._loading(false);
            
            if(oData.code == 0)
                $('#wall-event-' + oData.id + ', #wall-event-' + oData.id + ' + .wall-divider-nerrow').bxwallanim('hide', $this._sAnimationEffect, $this._iAnimationSpeed, function() {
                    $(this).remove();
                    
                    if($('#bxwall > .wall-view > :last').is('.wall-divider-nerrow'))
                        $('#bxwall > .wall-view > :last').remove();
                });                        
        },
        'json'
        );
}

BxWallView.prototype.showPopup = function(sUrl) {
	
    var oPopupOptions = {
        fog: {
            color: '#fff', 
            opacity: .7
        } 
    };

    $.get(sUrl, function(data) {   
        $('#share_post_popup').remove();
        $(data).appendTo('body').dolPopup(oPopupOptions); 
    });

}

BxWallView.prototype.filterPosts = function(oLink) {
    var sId = $(oLink).attr('id');
    var sFilter = sId.substr(sId.lastIndexOf('-') + 1, sId.length);
    var oLoading = $('#bxwall > .paginate > .per_page_section > :last').get();

    //--- Change Control ---//
    $(oLink).parent().siblings('.active:visible').hide().siblings('.notActive:hidden').show().siblings('#' + sId + '-pas:visible').hide().siblings('#' + sId + '-act:hidden').show();

    this.getPosts(oLoading, 0, null, sFilter);
    this.getPaginate(oLoading, 0, null, sFilter);
}
BxWallView.prototype.changePage = function(iStart, iPerPage, sFilter) {
    var oLoading = $('#bxwall > .paginate > .per_page_section > :last').get();
        
    this.getPosts(oLoading, iStart, iPerPage, sFilter);
    this.getPaginate(oLoading, iStart, iPerPage, sFilter);
}
BxWallView.prototype.getPosts = function(oLoading, iStart, iPerPage, sFilter) {
    var $this = this;
    var oData = this._getDefaultData();
    if(iStart)
        oData['WallStart'] = iStart;
    if(iPerPage)
        oData['WallPerPage'] = iPerPage;
    if(sFilter)
        oData['WallFilter'] = sFilter;
        
    if(oLoading)
        this._loading(true);

    jQuery.post(
        this._sActionsUrl + 'get_posts/',
        oData,
        function(sResult) {
            $this._loading(false);
            
            $('#bxwall > .wall-view').bxwallanim('hide', $this._sAnimationEffect, $this._iAnimationSpeed, function() {
                $(this).html(sResult).bxwallanim('show', $this._sAnimationEffect, $this._iAnimationSpeed);
            });                        
        }
        );
}
BxWallView.prototype.getPaginate = function(oLoading, iStart, iPerPage, sFilter) {
    var $this = this;
    var oData = this._getDefaultData();
    if(iStart != undefined)
        oData['WallStart'] = iStart;
    if(iPerPage != undefined)
        oData['WallPerPage'] = iPerPage;
    if(sFilter)
        oData['WallFilter'] = sFilter;

    if(oLoading)
        this._loading(true);

    jQuery.post (
        this._sActionsUrl + 'get_paginate/',
        oData,
        function(sResult) {                                    
            $this._loading(false);

            $('#bxwall > .paginate').bxdolcmtanim('hide', $this._sAnimationEffect, $this._iAnimationSpeed, function() {
                if(sResult.length > 0) {
                    $(this).replaceWith(sResult);
                    $(this).bxdolcmtanim('show', $this._sAnimationEffect, $this._iAnimationSpeed);
                }
            });            
        }
        );
}

BxWallView.prototype._loading = function (bShow) {
    bxWallShowLoading($('#bxwall'), bShow);
}

BxWallView.prototype._getDefaultData = function () {
    return {
        WallOwnerId: this._iOwnerId
        };
}

BxWallView.prototype._err = function (oElement, bShow, sMessage) {    
    if (bShow && !$(oElement).next('.wall-post-err').length)
        $(oElement).after(' <b class="wall-post-err">' + sMessage + '</b>');
    else if (!bShow && $(oElement).next('.wall-post-err').length)
        $(oElement).next('.wall-post-err').remove();    
}


// #MODIFIED_TRL #09.08.2012 ## Hide Action Post ##
//--START
BxWallView.prototype.hideEvent = function(iPostId,me) {
    var $this=this;

    jQuery.getJSON(
        this._sActionsUrl + 'hideEvent/'+iPostId, 
        function(sResult) {                                    
            $this._loading(false);
            if (sResult.myvar){
                $(me).parents("div:eq(2)").prev(".wall-divider-nerrow:first").remove();
                $(me).parents("div:eq(2)").fadeOut(1000);
            }
    
        }
        );
}
//--END

// #MODIFIED_TRL #08.01.2012 ## Hide/Unhide Action Post ##
//--START

BxWallView.prototype.unHideEvent = function(iPostId,me) {
    var $this=this;

    jQuery.getJSON(
        this._sActionsUrl + 'unHideEvent/'+iPostId, 
        function(sResult) {                                    
            $this._loading(false);
            if (sResult.myvar){
                $(me).parents("div:eq(2)").prev(".wall-divider-nerrow:first").remove();
                $(me).parents("div:eq(2)").fadeOut(1000);
            }
    
        }
        );
}		
//--END	

// #MODIFIED_TRL #00.00.2013 ## Report abuse administration ##
//--START 
BxWallView.prototype.showPopup = function(iPostId,me) {
sUrl = site_url + 'm/wall/Rabuse_Popup/'+iPostId;
    var oPopupOptions = {
        fog: {
            color: '#fff', 
            opacity: .7
        },
        closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {
        $('#conten_rabuse').remove();
        $(data).appendTo('body').dolPopup(oPopupOptions); 
    });
}	

BxWallView.prototype.showPopupPost = function(iPostId,me) {
sUrl = site_url + 'm/wall/Rabuse_Popup_Post/'+iPostId;
    var oPopupOptions = {
        fog: {
            color: '#fff', 
            opacity: .7
        },
        closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {
        $('#conten_rabuse_post').remove();
        $(data).appendTo('body').dolPopup(oPopupOptions); 
    });
}	
//--END	

