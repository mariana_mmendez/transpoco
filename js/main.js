TrlHstManage = new TrlHst();

function TrlHst(){
	this.date_from=null;
    this.date_to=null;
    this.filter_word=null;
}

TrlHst.prototype.showPopup = function(sUrl) {

   var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});

}

TrlHst.prototype.getAddModuleForm = function(){
	sUrl = site_url + 'm/hashtags/get_add_module_popup';

	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.saveItem = function(sMessageWorn, sUrl, sRedirect){
	var Title = $('#kpi_title').val();	
	var AlertUnit = $('#kpi_alert_unit').val();
	var AlertAction = $('#kpi_alert_action').val();
   
	if (!Title.length || !AlertUnit.length || !AlertAction.length) 
	{		
		alert(sMessageWorn); 
		return false;
	}	
   
	var oDate = new Date();
	$.post(sUrl,		
			{
				title:Title,
				alertunit:AlertUnit,
                                alertaction:AlertAction,
				/*action: $('#kpi_action').val(),*/
				enabled:$('#kpi_enabled').is(':checked')
			},
		function(oData){
			alert(oData.msg);
			if (oData.redirect)
				window.location = sRedirect;  
		},
		'json'
	);
}
TrlHst.prototype.deleteItem = function(id){
	sUrl = site_url + 'm/hashtags/get_delete_item_popup/'+id;

	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.deleteItemYes = function(id){
	sUrl = site_url + 'm/hashtags/delete_item/'+id;
	$.post(sUrl,		
			{},
		function(oData){
			alert(oData.msg);
			if (oData.redirect)
				location.reload();  
		},
		'json'
	);
}



TrlHst.prototype.moduleExists = function(sMessageWorn){
	var uri = $('#hashtags_uri').val();

	sUrl = site_url + 'm/hashtags/module_exists/'+uri;
       	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.saveModule = function(sUri){
	sUrl = site_url + 'm/kpi/save_module/'+sUri;
        alert("Entro");
	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.createManualkpi = function(){
	var sNickName = $('#kpi_nickname').val();
	
	sUrl = site_url + 'm/kpi/create_manual_kpi/'+sNickName;
	
	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.executekpi = function(sNickName){
	var sNickName = $('#kpi_nickname').val();
	
	sUrl = site_url + 'm/kpi/create_manual_kpi/'+sNickName+'/'+'1';
	
	var oPopupOptions = {
        fog: {color: '#fff', opacity: .7},
		closeOnOuterClick: false        
    };

    $.get(sUrl, function(data) {   
		 $('#hashtags_popup').remove();
		  $(data).appendTo('body').dolPopup(oPopupOptions); 
	});
}

TrlHst.prototype.downloadBackup = function(iId){
	sUrl = site_url + 'm/kpi/download_backup/'+iId;	
	window.open(sUrl, '');

}

TrlHst.prototype.saveAllItems = function(){
    	var bReadyToExit = false;
	$("[id^=row_]").each( function() {
		bReadyToExit = false;
		var oItem = $(this);
		var iItemId = oItem.attr('id').replace('row_', '');
		var url = site_url + 'm/kpi/save_items/' + iItemId;
		if (!oItem.find('input[name="unit"]').val().length || !oItem.find('input[name="action"]').val().length) 
                 {
                     alert('Fill the fields are required!!'); 
		return false;
                 }
                
		
		$.ajax({
			url: url,
			type: 'post',
			cache: false,
			data: {
				alertunit: oItem.find('input[name="unit"]').val(),
				alertaction: oItem.find('input[name="action"]').val(),
				active: oItem.find('input[name="active"]').is(':checked')
			},	
			async: false,
			success: function(){
				bReadyToExit = true;
			}
		});
		
	});
	
	if (bReadyToExit) location.reload();
}

TrlHst.prototype.generatereport = function(from,to,vhur,type){
   
       
        sUrl = site_url + 'm/kpi/generate_report/';
        var from_date=$('input[name="'+from+'"]').val();
        var to_date=$('input[name="'+to+'"]').val();
        var kpivhur=$('input[name="'+vhur+'"]').val();
        if(!from_date.length || !to_date.length  )
               alert("Required Date Range");
        else
            {
               
                //$(location).attr({href:sUrl, target:"_blank"});
               window.open(sUrl+"?kpi_date_from="+from_date+"&kpi_date_to="+to_date+"&kpi_vhur="+kpivhur+"&type="+type,'_blank');
            }
        
	
}

TrlHst.prototype.generateNps=function(me,sTitle,bLoadGroupButton,bActiveButtonNPS){
    if(!bLoadGroupButton)
        bLoadGroupButton=false;
    var sUrl=site_url+'m/hashtags/get_nps/';
    this.date_from=$('input[name="hfa_date_from"]').val();
    this.date_to=$('input[name="hfa_date_to"]').val();
	
	var lengthInputs = document.getElementsByName("hashtags_to_graphic[]").length;
	this.hashtags_to_graphic= [];
	
	var hashtags = [];
	for( var i=0; i<lengthInputs; ++i ) {
		hashtags[i] = document.getElementsByName("hashtags_to_graphic[]")[i].value;
	}
	
    var sTypeFilterNps=$(me).attr('id');
    $this=this;
   // $this.manageButtton(me,'group-button-3');   
    $this.bx_loading_hfa("loading-report-hfa", true);
    $.ajax({
        url:sUrl,
        dataType: 'json',
        type: "POST",
        data:{
            date_from:$this.date_from,
            date_to:$this.date_to,
            typeFilter:sTypeFilterNps,
            bLoadGroupButton:bLoadGroupButton,
			hashtags:hashtags.toString()
        },
        success: function(data){            
            $this.bx_loading_hfa("loading-report-hfa", false);  
            $this.cleanDivHtmlForReport();
            if(data.status)
            {      
				//alert('si');
                  //$(".group-button").fadeIn('slow');
                //if(bLoadGroupButton)
                    //$("#group-button-option").html(data.groupTypeFilter).fadeIn('slow');
                $this.getNps(sTitle,data.dataGraphics);
                //$(".group-button-3").fadeIn('slow');
                $("#table-detail").html(data.dataTable);
               // $this.readyDataTable('table_hfa_report');
            }
            else{
                $("#container-report-hfa").html(data.msg_error);
            }                                  
        }            
    });
}
/*
 *Function Get NPS for (Day,Week,Month,Year)
 */
var chart;
TrlHst.prototype.getNps=function(sTitle,objData) {
  
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container-report-hfa',
            type: 'line'
        },
        title: {
            text: objData.sTitleChar
        },
        /*subtitle: {
                text: 'Source: WorldClimate.com'
            },*/
        xAxis: {
            categories: objData.categories,
            //             min: 10,
            labels: {
                rotation: -90,
                align: 'top',
                style: {
                    fontSize: '11px',
                    fontFamily: 'Verdana, sans-serif'
                },
                y:objData.iPositionYForLabel
                
            }
        //categories:objData.
        },
        /*       scrollbar: {
        enabled: true
    },*/
        legend: {
			layout: 'horizontal',
            align: 'top',
			verticalAlign: 'top',
            y: 25
            
        },
        yAxis: {
            title: {
                text: objData.sNameLegendY
            },
            //max:100,
            min:0,
			allowDecimals:false,
            labels: {
                format: '{value}'
            }
        },
        tooltip: {
            enabled: true,
              
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                this.x +': '+ this.y +' ';
            }
        },
        plotOptions: {
                series: {
                    cursor: 'pointer',
                  /*  point: {
                        events: {
                            click: function() {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: this.pageX,
                                        y: this.pageY
                                    },
                                    headingText: this.series.name,
                                    maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
                                        this.y +' visits',
                                    width: 200
                                });
                            }
                        }
                    },*/
                    marker: {
                        lineWidth: 1
                    }
                }
            },
           
    
        series: objData.series
		// [{ 

            // name: objData.nameseries,
            // data: objData.dataseries
        // }]
 
    });
   
}

TrlHst.prototype.bx_loading_hfa=function(sId, b) {

    if (1 == b || true == b) {

        var e = $('<div class="loading_ajax_hfa"><div class="loading_ajax_rotating_hfa"></div></div>');
        $('#' + sId).prepend(e);
        e = $('#' + sId + " .loading_ajax_hfa");
        e.css('left','55%');
        e.css('top', '55%');
        

    } else {

        $('#' + sId + " .loading_ajax_hfa").remove();

    }
}

TrlHst.prototype.manageButtton=function(me,sClass){
    $('.'+sClass+ ' .active').removeClass('active').addClass('btn')
    $(me).addClass('active');
}

TrlHst.prototype.cleanDivHtmlForReport=function() {
    
    //Clean all Table
    $("#table-detail").html('');
    $("#table-detail-comparable-2").html('');
    //Clean all div for charts
    $("#container-report-hfa2").html('');
    $("#container-report-hfa").html('');
    
}

TrlHst.prototype.changeDates=function(aPeriod) {
	var aPeriods = [];
	aPeriods = aPeriod;	
	var period = document.getElementsByName('hashtag_period');
	var selectedperiod =period[0].options[period[0].selectedIndex].value;
	
	document.getElementsByName("hfa_date_from")[0].value = aPeriod[0] ;
	document.getElementsByName("hfa_date_to")[0].value = aPeriod[selectedperiod] ;	
}
