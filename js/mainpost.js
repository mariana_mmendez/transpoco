$.fn.bxwallanim = function(action, effect, speed, h) {    
   return this.each(function() {   		
   		var sFunc = '';
   		var sEval;

   		if (speed == 0)
   			effect = 'default';
   			
  		switch (action) {
  			case 'show':
  				switch (effect) {
  					case 'slide': sFunc = 'slideDown'; break;
  					case 'fade': sFunc = 'fadeIn'; break;
  					default: sFunc = 'show';
  				}  				
  				break;
  			case 'hide':
  				switch (effect) {
  					case 'slide': sFunc = 'slideUp'; break;
  					case 'fade': sFunc = 'fadeOut'; break;
  					default: sFunc = 'hide';
  				}  				
  				break;  				
  			default:
  			case 'toggle':
  				switch (effect) {
  					case 'slide': sFunc = 'slideToggle'; break;
  					case 'fade': sFunc = ($(this).filter(':visible').length) ? 'fadeOut' : 'fadeIn'; break;
  					default: sFunc = 'toggle';
  				}  				  			  				
  		}
  		
  		if ((0 == speed || undefined == speed) && undefined == h) {
  			sEval = '$(this).' + sFunc + '();';
  		}
  		else if ((0 == speed || undefined == speed) && undefined != h) {
  			sEval = '$(this).' + sFunc + '(); $(this).each(h);';
  		}
  		else {
  			sEval = '$(this).' + sFunc + "('" + speed + "', h);";
  		}
  		eval(sEval);
  		
  		return this;
   });  
};
function bxWallShowLoading(oParent, bShow) {
    var oLoading = oParent.children('.wall-loading');
    
    if(bShow) {
        oLoading.css('left', (oParent.width() - oLoading.width())/2);
        oLoading.css('top', (oParent.height() - oLoading.height())/2);
        oLoading.show();
    }
    else
        oLoading.hide();
}



//AGREGADO para el taguear people on publications




function tagPeople(oText, event){
	var oRealValue = $('input[name$="realvalue"]');
	oRealValue.val(oRealValue.val()+String.fromCharCode(event.keyCode));
	console.log(event);
	
	var oTagger = $('#member-to-tag');
	
	if (!oTagger.length) { $('<div id="member-to-tag" style="display: none;"></div>').prependTo('body'); }

	var sText = oText.value;
		
	aLetters = sText.split(' ');
		
	$.each(aLetters, function(index, letter){
		
		if (index == (aLetters.length - 1)){
		
			
			$.ajax({
				type: "POST", 
				url: site_url+'modules/boonex/wall/autotag.php', 
				data: 'text=' + letter,
				success: function(data){
					
					oTagger.html(data).dolPopup({
						position: $(oText).get(), 
						speed:0 
					});
					
				}
			});
		}
	 });
		
	$(oTagger).css('margin-top', $(oText).height());
}

function addMention(oUser){
	var oForm = $('#WallPostText');
	var aValues = $(oUser).attr('id').split('_');
	
	oForm.prepend('<input class="form_input_hidden" type="hidden" name="mentions[]" value="'+aValues[0]+'">');
	
	if(!oForm.find('.mentions-on-this').length)	oForm.prepend('<div class="mentions-on-this"><span>People Tagged: </span></div>');
	
	oForm.find('.mentions-on-this').append('<span class="metion"><span>'+aValues[1]+'</span><span class="remove-tag"></span></span>');
	$('#member-to-tag').html('');
	var oTextArea = oForm.find('textarea');
	var oRealValue = $('input[name$="realvalue"]');
	
	var sText = oTextArea.val();
	
	aLetters = sText.split(' ').reverse();
	aLetters[0] = aValues[1];
	sTextArea = aLetters.reverse().join(' ');
	
	sText = oTextArea.val();
	
	aLetters = sText.split(' ').reverse();
	aLetters[0] = '@['+aValues[0]+']';
	sTextArea = aLetters.reverse().join(' ');
		
	oTextArea.val(sTextArea);
	

	$('input[name$="realvalue"]').val(sMention);
}


