<?php

bx_import('BxDolModuleDb');

class TrlHstDb extends BxDolModuleDb {

    var $_oMain;
    var $_sPrefix;
    var $_oConfig;

    /*
     * Constructor.
     */

    function TrlHstDb(&$oConfig) {
        parent::BxDolModuleDb($oConfig);
        $this->_oConfig = &$oConfig;
        $this->_sPrefix = $this->_oConfig->getDbPrefix();
    }

	function getModulesSearch(){
		return $this->getAll("SELECT * FROM trl_hst_modules MO WHERE MO.mod_active=1 ORDER BY MO.mod_order ASC");			
	}
	
	function getHashtagsText($iObjId, $sNameTable, $sFieldTable){
		if($sFieldTable == 'cmt_text'){			
			$sWhereField = 'cmt_id';
		}else{
			$sWhereField = 'id';
		}
	return $this->getOne("SELECT {$sFieldTable} FROM {$sNameTable} CM WHERE {$sWhereField} = {$iObjId} ");					
	}
	
	function getHashtagInfo($iObjId, $sNameTable, $sFieldTable){
		if($sFieldTable == 'cmt_text'){			
			$sWhereField = 'cmt_id';
		}else{
			$sWhereField = 'id';
		}
		return $this->getRow("SELECT * FROM {$sNameTable} CM WHERE {$sWhereField} = {$iObjId} ");					
	}
	
	function getHashtagsObjects($Hashtag, $sOrder){
		
		switch ($sOrder) {
			case 'bymodule' :
				$sOrderBy = "MO.mod_order ASC, OB.obj_timestamp DESC";
			break;
			
			case 'all' :
				$sOrderBy = "OB.obj_timestamp DESC";
			break;	
		}
		
		return $this->getAll("SELECT OB.*, FROM_UNIXTIME(OB.obj_timestamp) AS 'obj_date', 
								MA.mat_field_table, MA.mat_name_table, MA.mat_alert_action,
								MO.mod_uri, MO.mod_id
								FROM trl_hst_objects OB 
								INNER JOIN trl_hst_hashtags HS ON HS.hst_id=OB.obj_hst_id
								INNER JOIN trl_hst_matrix MA ON MA.mat_name_table=OB.obj_event_type
								INNER JOIN trl_hst_modules MO ON MO.mod_id=MA.mat_mod_id
								WHERE HS.hst_name = '{$Hashtag}' AND 
								MO.mod_active = 1 
								ORDER BY {$sOrderBy} ");
		
	}
	
	function getHashtagsTrendingTopics($iLimit, $Type= 'cloud'){
		$aQuery="SELECT OB.obj_hst_id,REPLACE(HS.hst_name, '#', '') as 'hst_name', COUNT(OB.obj_object_id) AS 'count_hst'
								FROM trl_hst_objects  OB 
								INNER JOIN trl_hst_hashtags HS ON HS.hst_id=OB.obj_hst_id
								WHERE OB.obj_timestamp > UNIX_TIMESTAMP()-(60*60*24*90)
								#AND  COUNT(OB.obj_object_id) > 5 
								GROUP BY OB.obj_hst_id
								ORDER BY COUNT(OB.obj_object_id) DESC  
								LIMIT {$iLimit}";
		
		switch($Type){
			case 'cloud':
				return $this->getPairs($aQuery,"hst_name","count_hst");
				break;
				
			case 'table':
				return $this->getAll($aQuery);
				break;
		}
		
		
		
	}
	
	function getHashtagsReportTable($sHstSearch, $dHstFrom,$dHstTo ){
		if(!isset($sHstSearch) or empty($sHstSearch) or $sHstSearch==''){
			$sExtraParamenter= " GROUP BY HS.hst_id";
		}else{
			$sExtraParamenter= "AND HS.hst_name='{$sHstSearch}' ";
		}
			
		$sQuery ="SELECT  
					HS.hst_id AS 'hashtag_id',
					HS.hst_name AS 'hashtag', 
					FROM_UNIXTIME(HS.hst_timestamp) AS 'start_date', 
					( SELECT  COUNT(obj_id) FROM trl_hst_objects 
						WHERE obj_event_type LIKE '%bx_photos_cmts%' AND obj_hst_id = HS.hst_id 
						AND  obj_timestamp >=  UNIX_TIMESTAMP('{$dHstFrom}') AND obj_timestamp <=  UNIX_TIMESTAMP('{$dHstTo}'))  
						AS 'total_photo_comments', 

					(SELECT  P.NickName 
						FROM trl_hst_objects  O
						INNER JOIN Profiles P ON O.obj_owner_id=P.ID
						WHERE O.obj_hst_id = HS.hst_id AND 
						O.obj_timestamp >= UNIX_TIMESTAMP('{$dHstFrom}') AND O.obj_timestamp <= UNIX_TIMESTAMP('{$dHstTo}')
						GROUP BY O.obj_owner_id
						ORDER BY COUNT(O.obj_object_id)  DESC
						LIMIT 1) AS 'user_most_tags',
						
					COUNT(OB.obj_id)  AS 'total_post',
					P.NickName 'user_that_started'
					FROM trl_hst_hashtags HS
					INNER JOIN trl_hst_objects OB ON OB.obj_hst_id=HS.hst_id
					INNER JOIN Profiles P ON HS.hst_owner_id=P.ID
					WHERE OB.obj_timestamp >=  UNIX_TIMESTAMP('{$dHstFrom}') AND OB.obj_timestamp <=  UNIX_TIMESTAMP('{$dHstTo}') ".$sExtraParamenter;	
					
		// echo $sQuery;
		return $this->getAll($sQuery);
					
	}
	
	
	function getHahstagsToGraphicsDb($sStringDateFrom, $sStringDateTo, $sHashtagsToGraphic){													
		return $this->getPairs("SELECT DATE_FORMAT((FROM_UNIXTIME(OB.obj_timestamp)),'%Y-%m-%d') as 'date' , COUNT(OB.obj_id)as'count'
								FROM trl_hst_objects OB
								INNER JOIN trl_hst_hashtags HS ON HS.hst_id=OB.obj_hst_id
								WHERE HS.hst_name='{$sHashtagsToGraphic}'
								AND  DATE_FORMAT((FROM_UNIXTIME(OB.obj_timestamp)),'%Y-%m-%d') >= '{$sStringDateFrom}'
								AND  DATE_FORMAT((FROM_UNIXTIME(OB.obj_timestamp)),'%Y-%m-%d') <= '{$sStringDateTo}'
								GROUP BY DATE_FORMAT((FROM_UNIXTIME(OB.obj_timestamp)),'%Y-%m-%d')","date","count");			
	}

	
    /*
     * Agregar Notificacion
     */

    function setNotification($optionAlert) {
		$sOptionAlert = json_encode($optionAlert);
		
		$aInfoMatriz = $this->getRow("SELECT * FROM trl_hst_matrix MA WHERE  MA.mat_id=86");
		
		var_dump($optionAlert);
        if ($optionAlert['nameAction'] == 'commentPost' || $optionAlert['nameAction'] == 'commentRemoved' || 
                $optionAlert['nameAction'] == 'commentUpdated' || $optionAlert['nameAction'] == 'commentRated')
            $Object = $optionAlert['aExtras'];
        else
            $Object = $optionAlert['iObject'];
        $Date = 'UNIX_TIMESTAMP();';
		if (getLoggedId() !=0)	{
			
			$this->query("INSERT INTO `trl_hst_hashtags` (`hst_name`, `hst_owner_id`, `hst_timestamp`) 
							VALUES ('{$sOptionAlert}', {$optionAlert['iowner']}, UNIX_TIMESTAMP() );");	
			$iLastID = $this->lastId();
			
			$this->query("INSERT INTO `trl_hst_objects` (`obj_event_type`, `obj_object_id`, `obj_hst_id`, `obj_owner_id`, `obj_timestamp`) 
							VALUES ('{$aInfoMatriz['mat_name_table']}', {$Object}, {$iLastID}, {$optionAlert['iowner']}, UNIX_TIMESTAMP() );");
		}
						
	}
	
	function getInfoTabla($idModule, $idAction){		
		return $this->getRow("SELECT * FROM trl_hst_matrix MA WHERE MA.mat_id = {$idAction} AND MA.mat_mod_id = {$idModule}");
	}
	
	function getComment($aInfoTabla,$Object, $IdTable){
		return $this->getOne("SELECT {$aInfoTabla['mat_field_table']} FROM {$aInfoTabla['mat_name_table']} WHERE {$IdTable} = {$Object}");
	}

	function getHashtagExist($sHashtagName){
		$sHashtagName = '#'.$sHashtagName;
		return $this->getOne("SELECT H.hst_id FROM trl_hst_hashtags H WHERE H.hst_name = CONVERT('{$sHashtagName}' USING utf8);");
	}
	
	function insertNewHashtag($sHashtag,$iOwner){
		$sHashtag = '#'.$sHashtag;
		$this->query("INSERT INTO `trl_hst_hashtags` (`hst_name`, `hst_owner_id`, `hst_timestamp`) 
			VALUES ('{$sHashtag}', {$iOwner}, UNIX_TIMESTAMP() );");
		$iIDHashtagExist = $this->lastId();
		return $iIDHashtagExist;
	}
	
	function insertNewObjectHashtag($sInfoTabla, $Object, $iIDHashtagExist, $iOwner){
		$this->query("INSERT INTO `trl_hst_objects` (`obj_event_type`, `obj_object_id`, `obj_hst_id`, `obj_owner_id`, `obj_timestamp`) 
							VALUES ('{$sInfoTabla}', {$Object}, {$iIDHashtagExist}, {$iOwner},  UNIX_TIMESTAMP() );");		
	}
	
    function getModules() {
        $aModules = $this->getColumn("SELECT `mod_uri` as `uri` FROM `{$this->_sPrefix}modules` ORDER BY mod_uri");
        $aSysModules = $this->getSysModules();

        foreach ($aSysModules as $sUri) {
            if ($sUri == 'hashtags')
                continue;

            if (!in_array($sUri, $aModules))
                $this->insertModule($sUri);
        }

        return $this->getAll("SELECT a.mod_id as `id`, m.title , m.uri as `uri` FROM `{$this->_sPrefix}modules` a INNER JOIN `sys_modules` m ON m.uri = a.mod_uri ORDER BY a.mod_active desc, m.uri");
    }

    function getSysModules() {
        return $this->getColumn("SELECT uri FROM `sys_modules` ORDER BY uri");
    }

    function getItems($iId) {
        return $this->getAll("SELECT * FROM `{$this->_sPrefix}matrix` m WHERE m.mat_mod_id = $iId");
    }

    function insertModule($sUri) {
        $this->query("INSERT INTO `{$this->_sPrefix}modules` SET mod_uri = '$sUri';");
    }

    /*
     * Extraer ID Modulo
     */

    function getIdModule($action, $unit) {
        return $this->getOne("SELECT mat_mod_id FROM {$this->_sPrefix}matrix WHERE mat_alert_action ='" . $action . "'AND mat_alert_unit='" . $unit . "'");
    }

    /*
     * Extraer ID Action
     */

    function getIdAction($action, $unit) {
        return $this->getOne("SELECT mat_id FROM {$this->_sPrefix}matrix WHERE mat_alert_action ='" . $action . "' AND mat_alert_unit='" . $unit . "'");
    }

	function getNameAction($action, $unit) {
        return $this->getOne("SELECT mat_title FROM {$this->_sPrefix}matrix WHERE mat_alert_action ='" . $action . "' AND mat_alert_unit='" . $unit . "'");
    }
	
    function getModule($mixed) {
        if ((int) $mixed)
            $sField = 'mod_id';
        else
            $sField = 'mod_uri';

        return $this->getFirstRow("SELECT m.mod_id, s.title, s.uri FROM `{$this->_sPrefix}modules` m INNER JOIN `sys_modules` s ON s.uri = m.mod_uri WHERE m.$sField = '$mixed'");
    }

    function getModuleTitle($mixed) {
        if ((int) $mixed)
            $sField = 'id';
        else
            $sField = 'uri';

        $aRes = $this->getFirstRow("SELECT m.title FROM `sys_modules` m INNER JOIN trl_kpi_modules a ON a.uri = m.uri WHERE a.$sField = '$mixed';");
        return strip_tags($aRes['title']);
    }

    function validateModuleToAdd($sUri) {
        if (!$this->getOne("SELECT COUNT(*) FROM `sys_modules` WHERE `uri` = '$sUri';"))
            return 'mod_not_exists';

        if ($this->getOne("SELECT COUNT(*) FROM `{$this->_sPrefix}modules` WHERE `mod_uri` = '$sUri';"))
            return 'mod_already_exits';

        return 'ok';
    }

    function saveModule($sUri) {

        return $this->query("INSERT INTO {$this->_sPrefix}modules SET uri = '$sUri';");
    }

    function existsItem($aData, $iModId) {
        return $this->getOne("SELECT COUNT(*) FROM `{$this->_sPrefix}matrix` m WHERE m.`mat_mod_id` = $iModId AND m.`mat_alert_unit` = '{$aData['alertunit']}' AND m.`mat_alert_action` = '{$aData['alertaction']}'");
    }

    function saveItem($aData, $iModId) {
        $aMod = $this->getModule($iModId);
        $sTitleKey = '_trl_kpi_' . $aMod['uri'] . '_' . $aData['alertunit'] . '_' . $aData['alertaction'];
        $alertunit = $aData['unit'];
        $alertaction = $aData['action'];
        $iActive = $aData['enabled'] == 'true' ? 1 : 0;

        if ($this->query("INSERT INTO {$this->_sPrefix}matrix SET mat_title = '$sTitleKey', mat_mod_id = $iModId, mat_alert_unit='" . $aData['alertunit'] . "',mat_alert_action='" . $aData['alertaction'] . "', mat_active = $iActive")) {
            $id = $this->getLangCategory();
            if ($id && $aData['title'])
                addStringToLanguage($sTitleKey, $aData['title'], -1, $id);
            $idHandler = $this->getFirstRow("select id from sys_alerts_handlers where name='hashtags_assign_alert'");
            $this->query("INSERT INTO sys_alerts SET  unit='" . $aData['alertunit'] . "', action='" . $aData['alertaction'] . "', handler_id=" . $idHandler['id']);
            return true;
        }

        return false;
    }

    function getLangCategory() {
        return (int) $this->getOne("SELECT `id` FROM `sys_localization_categories` WHERE `Name` = 'KPI' LIMIT 1");
    }

    function updateItem($aData, $iItemId) {
        $id_sys_alerts = $this->getIdTableSysAlerts($iItemId);
        $iActive = $aData['active'] == 'true' ? 1 : 0;
        $this->query("UPDATE `{$this->_sPrefix}matrix` SET `alert_unit` = '{$aData['alertunit']}', `alert_action` =  '{$aData['alertaction']}', `active` = $iActive WHERE `id` = $iItemId");
        $this->query("UPDATE  sys_alerts SET  unit='" . $aData['alertunit'] . "', action='" . $aData['alertaction'] . "' where id=" . $id_sys_alerts);
    }

    function deleteItem($id) {
        $id_sys_alerts = $this->getIdTableSysAlerts($id);
        $this->query("DELETE FROM sys_alerts WHERE id =$id_sys_alerts");
        return $this->query("DELETE FROM `{$this->_sPrefix}matrix` WHERE mat_id = $id");
    }

    function getIdTableSysAlerts($id) {
        //ID manejador de Alertas
        $idHandler = $this->getFirstRow("select id from sys_alerts_handlers where name='hashtags_assign_alert'");
        //Extraer los datos antiguos
        $oldDatos = $this->getFirstRow("select * from `{$this->_sPrefix}matrix` where mat_id=" . $id);
        //Extraer ID sys_alerts la cual se actualizara
        $id_sys_alerts = $this->getFirstRow("select id from sys_alerts where unit='" . $oldDatos['mat_alert_unit'] . "' AND action='" . $oldDatos['mat_alert_action'] . "' AND  handler_id=" . $idHandler['id']);
        return $id_sys_alerts['id'];
    }

    function generateReport($from, $to, $VHUR, $type) {
        if ($VHUR != '')
            if ($type == 'curren')
                $where = "where tkt.Date BETWEEN $from AND  $to AND p.ID_VHUR='$VHUR'";
            else
                $where = "where tkt.Date BETWEEN '$from' AND  '$to' AND p.ID_VHUR='$VHUR'";

        else
        if ($type == 'curren')
            $where = "where tkt.Date BETWEEN " . $from . " AND  " . $to;
        else
            $where = "where tkt.Date BETWEEN '" . $from . "' AND  '" . $to . "'";

        if ($type == 'curren') {


            return $this->getAll("  SELECT p.ID_VHUR as vhur, p.NickName,p.Full_name,tkl.title as title_action,sm.title as module, tpp.pos_name,tuu.uni_name, tuu2.uni_name as super_unit,tkt.Date
                                FROM Profiles p 
                                INNER JOIN {$this->_sPrefix}track tkt     ON tkt.Owner=p.ID
                                INNER JOIN trl_pos_positions tpp ON tkt.Owner_position = tpp.pos_id
                                INNER JOIN trl_uni_units tuu     ON tkt.Owner_unit = tuu.uni_id
                                INNER JOIN trl_uni_units tuu2    ON tkt.Owner_superunit = tuu2.uni_id
                                INNER JOIN {$this->_sPrefix}matrix tkl    ON tkt.Action= tkl.id
                                INNER JOIN {$this->_sPrefix}modules tkm	ON tkt.Module=tkm.id
                                INNER JOIN sys_modules sm        ON sm.uri=tkm.uri
                                $where 
                                order by tkt.Date DESC ");
        }
        if ($type == 'old') {
            return $this->getAll(" SELECT p.NickName,p.Full_name,tkl.Name  as title_action,sm.title as module, tpp.pos_name,tuu.uni_name, tuu2.uni_name as super_unit,tkt.Date
                                FROM Profiles p 
                                INNER JOIN trl_kpi_track tkt     ON tkt.Owner=p.ID
                                INNER JOIN trl_pos_positions tpp ON tkt.Owner_position = tpp.pos_id
                                INNER JOIN trl_uni_units tuu     ON tkt.Owner_unit = tuu.uni_id
                                INNER JOIN trl_uni_units tuu2    ON tkt.Owner_superunit = tuu2.uni_id
                                INNER JOIN trl_actions tkl    ON tkt.Action= tkl.id
                                INNER JOIN trl_kpi_modules tkm	ON tkt.Module=tkm.id
                                INNER JOIN sys_modules sm        ON sm.uri=tkm.uri
                                $where 
                                 order by tkt.ID");
        }
    }
}
?>