<?php

bx_import('BxDolModule');
bx_import('BxDolPageView');
bx_import('BxDolPaginate');

define('BX_WALL_FILTER_ALL', 'all');
define('BX_WALL_FILTER_OWNER', 'owner');
define('BX_WALL_FILTER_OTHER', 'other');
define('BX_WALL_FILTER_HIDE', 'hide');

define('BX_WALL_MEDIA_CATEGORY_NAME', 'wall');
 
class TrlHstModule extends BxDolModule {
    /*
     * ID de la accion en la cual se esta haciendo alerta
     */

    var $sUnit;
    /*
     * ID accion
     */
    var $sAction;
    /*
     * Nombre de la accion, se utiliza para validar si la accion 
     * se esta efectuando sobre los comentarios (Solo se usa para Comentarios)
     */
    var $nameAction;
    /*
     * Id del Objeto en el cual se realizo una accion
     */
    var $iObject;
    /*
     * Id Extra (Retorna el ID del comentario, que se genera con el AutoIncrement)
     */
    var $aExtras;
    /*
     * ID del Usuario el cual realiza una accion
     */
    var $iowner;
    /*
     * ID Position del Usuario Actual
     */
    var $iownerposition;
    /*
     * ID Unidad a la que pertenece el Usuario Actual
     */
    var $iownerunit;
    /*
     * ID Departamento de el Usuario Actual
     */
    var $iownersuperunit;

    /**
     * Constructor
     */
    function TrlHstModule($aModule) {
        parent::BxDolModule($aModule);
		$this->_sJsPostObject = 'oWallPost';
        $this->_sJsViewObject = 'oWallView';
        $this->iowner = $GLOBALS['logged']['member'] || $GLOBALS['logged']['admin'] ? $_COOKIE['memberID'] : 0;
    }	
	
	
	function actionAdministration($sUrl = 'manage') {
        if (!$this->isAdmin()) {
            $this->_oTemplate->displayAccessDenied();
            return;
        }
		
        $this->_oTemplate->pageStart();

        $aMenu = array(
            'manage' => array(
                'title' => _t('_trl_kpi_admin_manage'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/manage',
                '_func' => array('name' => 'getManageModulePanel', 'params' => array()),
            ),
            'report' => array(
                'title' => _t('_trl_kpi_admin_report'),
                'href' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/report',
                '_func' => array('name' => 'getReportModulePanelHst', 'params' => array()),
            )
        );

        $aMenu[$sUrl]['active'] = 1;

        $sContent = call_user_func_array(array($this->_oTemplate, $aMenu[$sUrl]['_func']['name']), $aMenu[$sUrl]['_func']['params']);

        echo $this->_oTemplate->adminBlock($sContent, $aMenu[$sUrl]['title'], $aMenu);

        $this->_oTemplate->addAdminCss(array('header_caption.css', 'admin.css', 'forms_extra.css', 'forms_adv.css', 'general.css', 'popup.css'));
        $this->_oTemplate->addAdminJs(array('main.js', 'jquery.dolPopup'));
        $this->_oTemplate->pageCodeAdmin(_t('_trl_hst_administration'));
        $this->_oDb->cleanCache('sys_alerts');
    }
	
	
	
	
	

    /*
     * Agregar notificacion por cada accion
     */

    function setNotification(){
        $idModule = $this->getIdModule();
        $idAction = $this->getIdAction();
		$nameActionFull = $this->getNameAction();
        $optionAlert = array(
            'iModID' => $idModule,
            'iAction' => $idAction,
            'nameAction' => $this->nameAction,
            'iObject' => $this->iObject,
            'iowner' => $this->iowner,
            'iownerposition' => $this->iownerposition,
            'iownerunit' => $this->iownerunit,
            'iownersuperunit' => $this->iownersuperunit,
            'aExtras' => $this->aExtras
        );
		
		echo '<br>';print_r($optionAlert);echo '<br>';		
		
		$aInfoTabla = $this->_oDb->getInfoTabla($idModule, $idAction);		
		if ($optionAlert['nameAction'] == 'commentPost' || $optionAlert['nameAction'] == 'commentRemoved' || $optionAlert['nameAction'] == 'post'){
            $Object = $optionAlert['aExtras'];
        }else{
            $Object = $optionAlert['iObject'];
		}
				
		if ($optionAlert['nameAction'] == 'commentPost'){
           $IdTable = 'cmt_id';
        }elseif($optionAlert['nameAction'] == 'post'){
           $IdTable = 'id'; 
		}
		
		if( $optionAlert['nameAction'] == 'post'){
			$iOwnerHst = $optionAlert['iObject'];
		}else{
			$iOwnerHst = $optionAlert['iowner'];
		}
		
		
		$sCommentFromAlert = $this->_oDb->getComment($aInfoTabla,$Object,$IdTable);	
		$iResult =  preg_match_all("/#(\p{L}[\p{L}_0-9]+|[0-9]+[\p{L}_]+)/iu", strip_tags($sCommentFromAlert), $matches);
		
		print_r($aInfoTabla);echo '<br>';print_r($sCommentFromAlert);echo '<br>';var_dump($iResult);echo '<br>';		

		if($iResult){
			$iCountArray = count($matches);
			$matches = array_unique($matches[1]);
			foreach($matches as $match){
				echo '<br>Existen hashtags<br>';
				print_r($match);
				echo '<br>';				
				$iIDHashtagExist = $this->_oDb->getHashtagExist($match);				
				if(!$iIDHashtagExist){
					$iIDHashtagExist = $this->_oDb->insertNewHashtag($match,$iOwnerHst);					 
				}
				$this->_oDb->insertNewObjectHashtag($aInfoTabla['mat_name_table'], $Object, $iIDHashtagExist,$iOwnerHst);
			}
		}else{
			echo '<br>NOOOO Existen hashtags<br>';
		}		
    }

    function setAlert($aAlert) {
        $this->sUnit = $aAlert['sUnit'];
        $this->sAction = $aAlert['sAction'];
        $this->nameAction = $aAlert['sAction'];
        $this->iObject = $aAlert['iObject'];
        $this->iowner = $aAlert['iowner'];
        $this->iownerposition = $aAlert['iownerposition'];
        $this->iownerunit = $aAlert['iownerunit'];
        $this->iownersuperunit = $aAlert['iownersuperunit'];
        
		if ($aAlert['sAction'] == 'commentPost' || $aAlert['sAction'] == 'commentRemoved'){            
			$this->aExtras = $aAlert['aExtras']['comment_id'];
        }elseif($aAlert['sAction'] == 'post'){
            $this->aExtras = $aAlert['aExtras']['event_id'];
		}else{
			$this->aExtras = $aAlert['aExtras'];
		}
		
    }

    /*
     * Get ID Module
     */

    function getIdModule() {
        $sUnit = $this->getModule();
        return $sUnit;
    }

    function getModule() {
        return $this->_oDb->getIdModule($this->sAction, $this->sUnit);
    }

    /*
     * Get ID Action
     */

    function getIdAction() {
        $sUnit = $this->getAction();
        return $sUnit;
    }

	function getNameAction() {
        $sUnit = $this->_oDb->getNameAction($this->sAction, $this->sUnit);
        return $sUnit;
    }
    function getAction() {
        return $this->_oDb->getIdAction($this->sAction, $this->sUnit);
    }

    function processAlert() {
        $iActionId = $this->getActionId();
        $iModuleId = $this->getModuleId();
    }

    function isAdmin() {
        return isAdmin($this->iowner);
    }



    function actionGetAddModulePopup() {
        echo $this->_oTemplate->getAddNewModuleForm();
    }

    function actionSaveItem($iModId) {
        $aData = $_POST;
        $aRes = array();
        $bExists = $this->_oDb->existsItem($aData, $iModId);

        if ($bExists) {
            $aRes['msg'] = _t('_trl_kpi_item_exists');
            $aRes['redirect'] = false;

            echo json_encode($aRes);
            return;
        }

        if ($this->_oDb->saveItem($aData, $iModId)) {
            $aRes['msg'] = _t('_trl_kpi_item_saved');
            $aRes['redirect'] = true;
        } else {
            $aRes['msg'] = _t('_trl_kpi_item_save_error');
            $aRes['redirect'] = false;
        }

        echo json_encode($aRes);
        return;
    }

    function actionDeleteItem($id) {
        $aRes = array();

        if ($this->_oDb->deleteItem($id)) {
            $aRes['msg'] = _t('_trl_hst_item_deleted');
            $aRes['redirect'] = true;
        } else {
            $aRes['msg'] = _t('_trl_hst_item_deleted_error');
            $aRes['redirect'] = false;
        }

        echo json_encode($aRes);
        return;
    }

    function actionSaveItems($iItemId) {
        $this->_oDb->updateItem($_POST, $iItemId);
        echo 'ok';

        return;
    }

    function actionGetDeleteItemPopup($id) {
        echo $this->_oTemplate->getDeleteItemPopup($id);
    }

    function actionCreateAction($iId) {
        echo $this->_oTemplate->getAddNewActionForm($iId);
    }

    function actionSaveModule($sUri) {
        if ($this->_oDb->saveModule($sUri))
            $sMsg = _t('_trl_kpi_admin_add_module_success');
        else
            $sMsg = _t('_trl_kpi_admin_add_module_error');

        $aForm = array(
            'form_attrs' => array(
                'action' => '',
                'onsubmit' => "javascript: return false;"
            ),
            'table_attrs' => array(
                'cellpadding' => 0,
                'cellspacing' => 0,
            ),
            'inputs' => array(
                'msg' => array(
                    'type' => 'custom',
                    'content' => $sMsg,
                    'attrs' => array('id' => 'kpi_title'),
                ),
                'buttons' => array(
                    'type' => 'submit',
                    'name' => '_trl_kpi_ok',
                    'value' => _t('_trl_kpi_ok'),
                    'attrs' => array(
                        'onclick' => 'javascript:location.reload()',
                    )
                ),
            )
        );

        $oForm = new BxTemplFormView($aForm);
        echo PopupBox('hashtags_popup', _t('_trl_kpi_admin_add_module'), $oForm->getCode());
    }

    function actionModuleExists($sUri = '') {
        if ($sUri == '') {
            echo $this->_oTemplate->getModuleResult('empty', $sUri);
            return;
        }

        $sModRes = $this->_oDb->validateModuleToAdd($sUri);
        echo $this->_oTemplate->getModuleResult($sModRes, $sUri);
    }
	
    function actionGenerateReport() {

        if (!isset($_GET['kpi_date_from']) || !isset($_GET['kpi_date_to'])) {
            echo "Access Denied";
            return;
        } else {

            $kpi_date_from = $_GET['kpi_date_from'];
            $kpi_date_to = $_GET['kpi_date_to'];
            $ikpi_date_from = (int) strtotime($kpi_date_from);
            $ikpi_date_to = (int) strtotime($kpi_date_to);
            $type = $_GET['type'];
            if (isset($_GET['kpi_vhur']) && $_GET['kpi_vhur'] != '')
                $ivhur = $_GET['kpi_vhur'];
            else
                $ivhur = '';
            if ($type == 'curren'){
                $filas = $this->_oDb->generateReport($ikpi_date_from, $ikpi_date_to, $ivhur, $type);
                 $date = date('Y') . '_' . date('m') . '_' . date('d');
            }
            if ($type == 'old') {
                $filas = $this->_oDb->generateReport($kpi_date_from, $kpi_date_to, $ivhur, $type);
                 $date = date('Y') . '_' . date('m') . '_' . date('d') . '_' . $type;
            }
           
            header("Content-type: application/vnd.ms-excel; name='excel'");
            header("Content-Disposition: filename=Report_kpi_" . $date . ".xls");
            header("Pragma: no-cache");
            header("Expires: 0");


            if (!count($filas))
                echo "No results were found";
            else {
                echo "<table  
                                  
                            >
                            <thead> 
                                <tr  style='color: #222; font-weight: bold;'>
                                    <th colspan='9' style='border:#a9c6c9 1px solid;padding: 8px;'>Report KPI System  $kpi_date_from to $kpi_date_to | Number of results : " . count($filas) . "</th>
                                </tr>
                                <tr  style='color: #222;'>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>VHUR</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Username</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Full name</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Action</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Module</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Owner Position</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Owner Unit</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Owner Superunit</th>
                                    <th style='border:#a9c6c9 1px solid;padding: 8px;'>Date</th>
                                </tr>
                               </thead>
                               <tbody>";

                $i = 0;
                foreach ($filas as $datas) {

                    if ($type == 'curren')
                        $date = date("m/d/Y h:i:s A T", $datas['Date']);
                    if ($type == 'old')
                        $date = $datas['Date'];
                    if ($i % 2 == 0)
                        $color = "#d4e3e5";
                    else
                        $color = "#F1F7F8";

                    echo '<tr >
                                   <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['vhur'] . '
                                    </td>
                                    <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['NickName'] . '
                                    </td>
                                      <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                       ' . $datas['Full_name'] . '
                                    </td>
                                       <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . _t($datas['title_action']) . '  
                                    </td>
                                     <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['module'] . '
                                    </td>
                                     <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['pos_name'] . '
                                    </td>
                                      <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['uni_name'] . '
                                    </td>
                                      <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                        ' . $datas['super_unit'] . '
                                    </td>
                                      <td style="border:#a9c6c9 1px solid;padding: 8px;">
                                     ' . $date . '
                                    </td>

                               </tr>';
                    $i++;
                }
                echo '</tbody></table>';
            }
        }
    }

	
	
		
	function serviceGetHomeMain(){
		$sBreadcum = $_GET['hst'];
		if(!empty($sBreadcum)){
			$sBreadcum = '#'.$sBreadcum;	
		}
		
		$this->_oTemplate->addCss('view.css');		
		$aHashtagsObject = $this->_oDb->getHashtagsObjects($sBreadcum, 'bymodule');	
		$iFirstModule = 0;
		
		foreach($aHashtagsObject as $aHashtagObject){
			$iCurrentModule = $aHashtagObject['mod_id'];
			if($iFirstModule != $iCurrentModule){
				$iFirstModule = $aHashtagObject['mod_id'];
				$aVarDivider = array(
					'cpt_class' => 'hashtag-divider', 
					'cpt_icon_url' => '', 
					'content' => strtoupper($aHashtagObject['mod_uri'])
				);			
				echo $this->_oTemplate->parseHtmlByName('divider', $aVarDivider);				
			}			
			$sCommet =     $this->_oDb->getHashtagsText($aHashtagObject['obj_object_id'] , $aHashtagObject['mat_name_table'], $aHashtagObject['mat_field_table']);
			$aCommentInfo = $this->_oDb->getHashtagInfo($aHashtagObject['obj_object_id'] , $aHashtagObject['mat_name_table'], $aHashtagObject['mat_field_table']);
			$aInfouser = getProfileInfo($aHashtagObject['obj_owner_id']);
			
			if($aHashtagObject['mat_alert_action'] == 'commentPost'){
				$sAction = 'Post a comment on ';
				$sGoToText = 'Go to post of comment';
				if($aHashtagObject['mod_uri']=='wall'){
					$sUrl = 'page/onewallpost?post_id='.$aCommentInfo['cmt_object_id'] ;
				}else{
					$sUrl = '';
				}
			}else{
				if($aHashtagObject['mod_uri']=='wall'){
						$sUrl = 'page/onewallpost?post_id='.$aCommentInfo['id'] ;
						$sGoToText = 'Go To Post';	
				}else{
						$sUrl = '';
						$sGoToText = '';	
				}
				$sAction = _t("_trl_hst_post_on");				
			}
			
			$aVariables = array(
				'author_thumbnail' => get_member_icon($aHashtagObject['obj_owner_id']),
				'author_url' => getProfileLink($aHashtagObject['obj_owner_id']),
				'author_username' => $aInfouser['NickName'],
				'post_wrote' => $sAction,
				'post_module' => ucfirst($aHashtagObject['mod_uri']).' Module',
				'post_content' => $sCommet,
				'post_ago' => $aHashtagObject['obj_date'],
				'url_go_to_post' => $sUrl,
				'url_go_to_post_text' => $sGoToText,
			);			
			$aVarDivider = array(
				'cpt_class' => 'hashtag-divider-nerrow', 
				'cpt_icon_url' => '', 
				'content' => '');				
		    echo $this->_oTemplate->parseHtmlByName('divider', $aVarDivider);
			echo $this->_oTemplate->parseHtmlByName('hashtag_general_entry', $aVariables); 			
		}
	}
	
	function serviceGetHomeTopics($iLimit = 35){
		$aHashtagsTrendingTopics = $this->_oDb->getHashtagsTrendingTopics($iLimit,'cloud');
		ksort($aHashtagsTrendingTopics);
		
		bx_import(BxBaseTags);
		$oTags = new BxBaseTags;
		echo $oTags->getTagsView($aHashtagsTrendingTopics,'m/hashtags/view?hst={tag}');
	} 
	
	function serviceGetHomeTopicsTable($iLimit = 35){
		$aHashtagsTrendingTopics = $this->_oDb->getHashtagsTrendingTopics($iLimit,'table');
		$aHashtagsTrendingTopicsTable = array(
			'url'=> BX_DOL_URL_ROOT,
			'bx_repeat:hashtags_info_rows' =>$aHashtagsTrendingTopics		
		);		
		if(count($aHashtagsTrendingTopics) == 0 or empty($aHashtagsTrendingTopics)){
			echo MsgBox(_t('_Empty'));
		}else{
			echo $this->_oTemplate->parseHtmlByName('table_hashtags', $aHashtagsTrendingTopicsTable);		
		}
	}
	
	
	function actionHome(){
		$sBreadcum = $_GET['hst'];
		
		if(!empty($sBreadcum)){
			$sBreadcum = '#'.$sBreadcum;	
		}
		$GLOBALS['oTopMenu']->setCustomSubHeader($sBreadcum);
		$this->templateAction('PageHashtagsHome', _t('_trl_hst_page_home'));		
	}
	
	function actionView(){
		$sBreadcum = $_GET['hst'];
		$sBreadcum = '#'.$sBreadcum;
		$GLOBALS['oTopMenu']->setCustomSubHeader($sBreadcum);
		$this->templateAction('PageHashtagsView', _t('_trl_hst_page_home'));		
	} 
	
	function templateAction($sPage, $sPageName)
	{		
		bx_import($sPage, $this->_aModule);
		$sClass = $this->_aModule['class_prefix'] . $sPage;
		$oPage = new $sClass($this);
		$this->_oTemplate->pageStart();
		echo $oPage->getCode();
		$this->_oTemplate->pageCode($sPageName);
	}
	
	function actionReport() {
        $this->_oTemplate->pageStart();
		 $this->_oTemplate->addJs('main.js');
		$this->_oTemplate->getFormReportDataOfHst();        
        $this->_oTemplate->pageCodeAdmin(_t('_trl_hst_page_report'));	
    }
	
	function actionGraphic() {
        $this->_oTemplate->pageStart();
		
		
        $this->_oTemplate->addCss('admin.css');

        $this->_oTemplate->addJs('main.js');

        $this->_oTemplate->addJs('highcharts.src.js'); 
        $this->_oTemplate->addJs('exportingHighCharts.js');

		

        echo $this->_oTemplate->getReportModulePanelGrafics();        
        $this->_oTemplate->pageCode(_t('_trl_hst_page_grafic'));	
    }
	
	
	    //}
    function actionGetNps() {
		$sStringDateFrom = $_POST['date_from'];
        $sStringDateTo = $_POST['date_to'];
        $sStringTypeFilter = $_POST['typeFilter'];
        $aHashtags = $_POST['hashtags'];
		
		$aHashtagsToGraphic = explode(',',$aHashtags);		
		$aListSeries = $this->getListDay($sStringDateFrom, $sStringDateTo, 'day');
		
		foreach ($aListSeries as $sStringItem) {
			$iNps = 0.0;
			$aCategories[] = date('d-M-Y', strtotime($sStringItem));
		}
		$sStringDateFrom = date('Y-m-d', strtotime($sStringDateFrom));
		$sStringDateTo =   date('Y-m-d', strtotime($sStringDateTo));
		$i=0;
		foreach($aHashtagsToGraphic as $sHashtagsToGraphic){
			$aRresult = $this->_oDb->getHahstagsToGraphicsDb($sStringDateFrom, $sStringDateTo, $sHashtagsToGraphic);			
			
			$j=0;
			foreach ($aListSeries as $sListSeries) {
				if (array_key_exists($sListSeries, $aRresult)) {
					$data[$j]= (int)$aRresult[$sListSeries];
				}else{
					$data[$j]= 0;
				}
				
				$j++;
			}
			$aSeries[$i]['name'] = $sHashtagsToGraphic;
			$aSeries[$i]['data'] = $data;
			$i++;
		}	
		


		$aContentSeries['iMinNps'] = 0;
		$aContentSeries['iPositionYForLabel'] = 80;
		$aContentSeries['sTitleChar'] = 'Hashtag Diagram';
		$aContentSeries['sNameLegendY'] = 'Mentions';
		$aContentSeries['categories'] = $aCategories;
		$aContentSeries['series'] = $aSeries;

		$aDatas['status'] = true;
		$aDatas['dataGraphics'] = $aContentSeries;
		
        echo json_encode($aDatas);
    }
	
    function getListDay($sStringMinDay, $sStringMaxDay, $sStringType, $sStringDay = '') {       
        $aDayTemp = array();
        switch ($sStringType) {
            case 'day' :
                $iStart = $iDay = strtotime($sStringMinDay);
                $iEnd = strtotime($sStringMaxDay);
                while ($iDay <= $iEnd) {
                    $aDayTemp[] = date('Y-m-d', $iDay);
                    $iDay = strtotime("+1 day", $iDay);
                }
                break;
            case 'week' :
                $iStart = $iDay = strtotime($sStringMinDay);
               
                $iEnd = strtotime($sStringMaxDay);
                while ($iDay <= $iEnd) {
                    $aDayTemp[] = (int) date('W', $iDay) . '-' . date('Y', $iDay);
                    $iDay = strtotime("+1 week", $iDay);
                }
                break;
            case 'month' :
                $iStart = $iDay = strtotime($sStringMinDay);
                $iEnd = strtotime($sStringMaxDay);
                while ($iDay < $iEnd) {
                    $aDayTemp[] = date('F-Y', $iDay);
                    $iDay = strtotime("+1 month", $iDay);
                }
                break;
            case 'year' :
                $iStart = $iYear =date('Y',  strtotime($sStringMinDay));
              
                $iEnd =date('Y',  strtotime($sStringMaxDay));
               
                while ($iYear <= $iEnd) {
                    $aDayTemp[] = $iYear;
                    $iYear++;
                }
                break;
            case 'byday':
                $iStart = $iDay = strtotime($sStringMinDay);
                $iEnd = strtotime($sStringMaxDay);
                while ($iDay <= $iEnd) {
                    if (date('l', $iDay) == $sStringDay)
                        $aDayTemp[] = date('Y-m-d', $iDay);
                    $iDay = strtotime("+1 day", $iDay);
                }
                break;
        }
        return $aDayTemp;
    }
	
	function serviceGetViewMain(){
		$sBreadcum = $_GET['hst'];
		$sBreadcum = '#'.$sBreadcum;
		$this->_oTemplate->addCss('view.css');	
		$iPerPage = 15;			
		$iFirstModule = 0;
		$sFilter = BX_WALL_FILTER_ALL;
        $aTopMenu = array(
            'wall-view-all' => array('href' => 'javascript:void(0)', 'onclick' => 'javascript:' . $this->_sJsViewObject . '.filterPosts(this)', 'title' => _t('_wall_view_all'), 'active' => 1)
        );

		
		$sContent = $this->_getPosts('desc', $iStart, $iPerPage, $sFilter,$sBreadcum);
		ob_start();
		?>
        var <?= $this->_sJsViewObject; ?> = new BxWallView({
        sActionUrl: '<?= BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri(); ?>',
        sObjName: '<?= $this->_sJsViewObject; ?>',
        iOwnerId: <?= 0; ?>, 
        sAnimationEffect: 'fade',
        iAnimationSpeed: 'slow'
        });
        <?		

		
		$sJsContent = ob_get_clean();		
		$oPaginate = $this->_getPaginate($sFilter, 1);
        $aVariables = array(
            'content' => $sContent,
            'view_js_content' => $sJsContent,
            'view_js_object' => $this->_sJsViewObject,
            'paginate' => $oPaginate->getPaginate(),
			'url_plugin_datatable' => BX_DOL_URL_ROOT.'modules/transactel/hashtags/templates/base/datatable',
        );
		
		$this->_oTemplate->addCss('view.css');	
        $this->_oTemplate->addJs(array('mainpost.js', 'view.js'));		
		return array($this->_oTemplate->parseHtmlByName('view', $aVariables), $aTopMenu, array(), true, 'getBlockCaptionMenu');
	}
	
	function _getPosts($sOrder, $iStart, $iPerPage, $sFilter,$sBreadcum) {
		$aHashtagsObject = $this->_oDb->getHashtagsObjects($sBreadcum, 'all');	
				
		foreach($aHashtagsObject as $aHashtagObject){
			$iCurrentModule = $aHashtagObject['mod_id'];
			if($iFirstModule != $iCurrentModule){
				$iFirstModule = $aHashtagObject['mod_id'];
				$aVarDivider = array(
					'cpt_class' => 'hashtag-divider', 
					'cpt_icon_url' => '', 
					'content' => strtoupper($aHashtagObject['mod_uri'])
				);			
				echo $this->_oTemplate->parseHtmlByName('divider', $aVarDivider);				
			}			
			$sCommet =     $this->_oDb->getHashtagsText($aHashtagObject['obj_object_id'] , $aHashtagObject['mat_name_table'], $aHashtagObject['mat_field_table']);
			$aCommentInfo = $this->_oDb->getHashtagInfo($aHashtagObject['obj_object_id'] , $aHashtagObject['mat_name_table'], $aHashtagObject['mat_field_table']);
			$aInfouser = getProfileInfo($aHashtagObject['obj_owner_id']);
			
			if($aHashtagObject['mat_alert_action'] == 'commentPost'){
				$sAction = 'Post a comment on ';
				$sGoToText = 'Go to post of comment';
				if($aHashtagObject['mod_uri']=='wall'){
					$sUrl = 'page/onewallpost?post_id='.$aCommentInfo['cmt_object_id'] ;
				}else{
					$sUrl = '';
				}
			}else{
				if($aHashtagObject['mod_uri']=='wall'){
						$sUrl = 'page/onewallpost?post_id='.$aCommentInfo['id'] ;
						$sGoToText = 'Go To Post';
				}else{
						$sUrl = '';
						$sGoToText = '';
				}
				$sAction = _t("_trl_hst_post_on");
			}
			
			$aVariables = array(
				'author_thumbnail' => get_member_icon($aHashtagObject['obj_owner_id']),
				'author_url' => getProfileLink($aHashtagObject['obj_owner_id']),
				'author_username' => $aInfouser['NickName'],
				'post_wrote' => $sAction,
				'post_module' => ucfirst($aHashtagObject['mod_uri']).' Module',
				'post_content' => $sCommet,
				'post_ago' => $aHashtagObject['obj_date'],
				'url_go_to_post' => $sUrl,
				'url_go_to_post_text' => $sGoToText,
				'cpt_class' => 'hashtag-divider-nerrow', 
				'cpt_icon_url' => '', 
				'content' => ''
			);	
				
			$sContent .= $this->_oTemplate->parseHtmlByName('hashtag_general_entry2', $aVariables);		
		}
		return $sContent;
	}
	
	
	function _getPaginate($sFilter, $IsIndex) {
		$aPaginationNew = new BxDolPaginate(array(
                        'start' => 0,
                        'count' => 5,
                        'per_page' => 5,
                        'on_change_page' => $this->_sJsViewObject . '.changePage({start}, {per_page}, \'' . $sFilter . '\')',
                        'on_change_per_page' => $this->_sJsViewObject . '.changePerPage(this, \'' . $sFilter . '\')',
                        'page_reloader' => false,
                        'per_page_changer' => false,
                        'info' => true,
                        'page_links' => false,
                        'view_all' => false
			));
		return $aPaginationNew;
    }
	
	
}
?>