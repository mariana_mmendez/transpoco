<?php

bx_import('BxTemplFormView');
bx_import('BxTemplSearchResult');
bx_import('BxTemplSearchProfile');
bx_import('BxDolTwigTemplate');
bx_import('BxDolParams');

class TrlHstTemplate extends BxDolTwigTemplate {

    var $_ActiveMenuLink = '', $oSearchProfileTmpl;

    /**
     * Constructor
     */
    function TrlHstTemplate(&$oConfig, &$oDb) {
        parent::BxDolTwigTemplate($oConfig, $oDb);
		// parent::loadTemplates();
        $this->oSearchProfileTmpl = new BxTemplSearchProfile();
		$this->_aTemplates = array('hashtag_general_entry');
    }

    function getManageModulePanel() {


        $sContent = '';

        //--Add module botton
        $aForm = array(
            'inputs' => array(
                'header' => array(
                    'type' => 'block_header',
                    'caption' => '<div align="center"><a id="add_new_module" href="javascript:TrlHstManage.getAddModuleForm();">' . _t('_trl_hst_admin_add_module') . '</a></div>',
                ),
            ),
        );

        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();

        $aForm = array(
            'form_attrs' => array(
                'id' => 'hashtags-actions-from',
                'name' => 'hashtags-actions-form',
                'method' => 'post',
                'enctype' => '',
                'action' => '',
                'onsubmit' => 'javascript: return false;'
            ),
        );

        $aForm['inputs'] = array();

        $aModules = $this->_oDb->getModules();

        foreach ($aModules as $aModule) {
            $aForm['inputs'] = array_merge($aForm['inputs'], array(
                "header_{$aModule['uri']}" => array(
                    'type' => 'block_header',
                    'caption' => $this->getCaption($aModule),
                    'collapsable' => true,
                    'collapsed' => false
                ),
                "area_{$aModule['uri']}" => array(
                    'type' => 'custom',
                    'content' => $this->getItems($aModule['id']),
                    'colspan' => true,
                    'attrs_wrapper' => array('style' => 'float:none;')
                ),
                "bottons_{$aModule['uri']}" => array(
                    'type' => 'custom',
                    'content' => $this->getAddNewBotton($aModule['id']),
                    'colspan' => true,
                    'attrs_wrapper' => array('style' => 'float:none;')
                ),
                "end_{$aModule['uri']}" => array(
                    'type' => 'block_end'
                )
                    ));
        }

        $aForm['inputs'] = array_merge($aForm['inputs'], array(
            'items_save' => array(
                'type' => 'submit',
                'name' => 'kpi-save-items',
                'value' => _t('_trl_hst_save'),
                'attrs' => array(
                    'id' => 'kpi_save_items',
                    'style' => 'width:200px;',
                    'onclick' => 'javascript:TrlHstManage.saveAllItems();'
                )
            )
                ));

        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();
        return $sContent;
    }

    function getReportModulePanel() {
        $sContent = '';
        //--Add module botton
        $aForm = array(
            'inputs' => array(
                'header' => array(
                    'type' => 'block_header',
                    'caption' => '<div align="center">'. _t('_trl_hst_admin_type_curren_records') .' </div>',
                ),
            ),
        );
        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();
        $aForm = array(
            'form_attrs' => array(
                'id' => 'kpi-add-item',
                'name' => 'kpi-add-item',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => '',
                'onsubmit' => "javascript: TrlHstManage.generatereport('kpi_date_from','kpi_date_to','kpi_vhur','curren'); return false;"
            ),
            'params' => array(
                'db' => array(
                    'table' => 'me_blgg_posts',
                    'key' => 'id',
                    'submit_name' => 'submit_form',
                ),
            ),
            'inputs' => array(
                'from' => array(
                    'type' => 'datetime',
                    'name' => 'kpi_date_from',
                    'caption' => _t('_trl_hst_admin_report_from'),
                    'required' => true,
                    'attrs' => array('id' => 'kpi_date_from'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'to' => array(
                    'type' => 'datetime',
                    'name' => 'kpi_date_to',
                    'caption' => _t('_trl_hst_admin_report_to'),
                    'required' => true,
                    'attrs' => array('id' => 'kpi_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'username' => array(
                    'type' => 'text',
                    'name' => 'kpi_vhur',
                    'caption' => _t('_trl_hst_admin_report_search_vhur'),
                    'info' => _t('_trl_hst_admin_report_info_vhur'),
                    'attrs' => array('id' => 'kpi_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'submit' => array(
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_trl_hst_admin_report_submit'),
                    'colspan' => true,
                ),
            ),
        );


        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();
        $aForm = array(
            'inputs' => array(
                'header' => array(
                    'type' => 'block_header',
                    'caption' => '<div align="center">'. _t('_trl_hst_admin_type_old_records') .' </div>',
                ),
            ),
        );

        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();
        $aForm = array(
            'form_attrs' => array(
                'id' => 'kpi-add-item_old',
                'name' => 'kpi-add-item_old',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => '',
                'onsubmit' => "javascript: TrlHstManage.generatereport('kpi_date_from_old','kpi_date_to_old','kpi_vhur_old','old'); return false;"
            ),
            'params' => array(
                'db' => array(
                    'table' => 'me_blgg_posts',
                    'key' => 'id',
                    'submit_name' => 'submit_form',
                ),
            ),
            'inputs' => array(
                'from' => array(
                    'type' => 'datetime',
                    'name' => 'kpi_date_from_old',
                    'caption' => _t('_trl_hst_admin_report_from'),
                    'required' => true,
                    'attrs' => array('id' => 'kpi_date_from'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'to' => array(
                    'type' => 'datetime',
                    'name' => 'kpi_date_to_old',
                    'caption' => _t('_trl_hst_admin_report_to'),
                    'required' => true,
                    'attrs' => array('id' => 'kpi_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'username' => array(
                    'type' => 'text',
                    'name' => 'kpi_vhur_old',
                    'caption' => _t('_trl_hst_admin_report_search_vhur'),
                    'info' => _t('_trl_hst_admin_report_info_vhur'),
                    'attrs' => array('id' => 'kpi_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                    'db' => array(
                        'pass' => 'Xss',
                    ),
                ),
                'submit' => array(
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_trl_hst_admin_report_submit'),
                    'colspan' => true,
                ),
            ),
        );


        $oForm = new BxTemplFormView($aForm);
        $sContent .= $oForm->getCode();
        
        
        //   $sContent.=$this->getHTML();
        return $sContent; // output posts list
    }

    function getHTML() {
        $this->addAdminCss('admin.css');
        $aVars = array(
            'bx_repeat:module_actions' => $aItems
        );
        return $this->parseHtmlByName('admin_report', $aVars);
    }

    function getCaption($aM) {
        $aVars = array(
            'title' => '<b>' . strip_tags($aM['title']) . '</b>',
        );

        return $this->parseHtmlByName('header_caption', $aVars);
    }

    function getAddNewActionForm($iId) {

        if ($this->_oConfig == null)
            $this->_oConfig = BxDolModule::getInstance('TrlHstConfig');

        $sTitle = $this->_oDb->getModuleTitle($iId);
        // echo "<script>alert('".$sTitle."')</script>";

        $sWrongCreateMessage = addslashes(_t('_energy_action_empty_title_or_name'));

        $sUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'save_item/' . $iId;
        $sRedirect = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/manage';

        $aForm = array(
            'form_attrs' => array(
                'id' => 'hashtags-add-item',
                'name' => 'hashtags-add-item',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => '',
                'onsubmit' => "javascript: TrlHstManage.saveItem('{$sWrongCreateMessage}', '{$sUrl}', '{$sRedirect}'); return false;"
            ),
            'table_attrs' => array(
                'cellpadding' => 0,
                'cellspacing' => 0,
                'class' => 'buy-points-table'
            ),
            'inputs' => array(
                'title' => array(
                    'type' => 'text',
                    'name' => 'title',
                    'value' => '',
                    'required' => true,
                    'caption' => _t('_trl_hst_admin_title_action'),
                    'info' => _t('_trl_hst_admin_title_info'),
                    'attrs' => array('id' => 'kpi_title'),
                ),
                'alert_unit' => array(
                    'type' => 'text',
                    'name' => 'unit',
                    'value' => '',
                    'required' => true,
                    'caption' => _t('_trl_hst_admin_alert_unit'),
                    'info' => _t('_trl_hst_admin_table_info'),
                    'attrs' => array('id' => 'kpi_alert_unit'),
                ),
                'alert_action' => array(
                    'type' => 'text',
                    'name' => 'action',
                    'value' => '',
                    'required' => true,
                    'caption' => _t('_trl_hst_admin_alert_action'),
                    'info' => _t('_trl_hst_admin_field_info'),
                    'attrs' => array('id' => 'kpi_alert_action'),
                ),
//				'action' => array(
//						'type' => 'select',
//						'name' => 'kpi_action',
//						'value' => 'delete',
//						'values' => $this->getActionsForItems(),
//						'colspan' => false,
//						'caption' => _t('_trl_hst_action'),
//						'attrs' => array('id' => 'kpi_action'),
//				),
                'item_checked' => array(
                    'type' => 'checkbox',
                    'name' => 'kpi_enabled',
                    'checked' => true,
                    'caption' => _t('_trl_hst_enabled'),
                    'attrs' => array('id' => 'kpi_enabled'),
                ),
                'create' => array(
                    'type' => 'submit',
                    'name' => 'energy_create_action',
                    'value' => _t('_energy_admin_create_button'),
                    'attrs' => array('id' => 'energy_create_action')
                )
            )
        );

        $oForm = new BxTemplFormView($aForm);

        return PopupBox('hashtags_popup', _t('_trl_hst_admin_add_item', strip_tags($sTitle)), $oForm->getCode());
    }

    function getActionsForItems() {
        $a = array();
        $a[] = array('key' => 'delete', 'value' => _t('_trl_hst_delete'));
        $a[] = array('key' => 'reassign', 'value' => _t('_trl_hst_reassign'));
        return $a;
    }

    function getAddNewBotton($iId) {
        $this->_oConfig = BxDolModule::getInstance('TrlHstConfig');
        $sUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . "create_action/$iId";
        return '<div align="right"><a id="add_new_action_' . $iId . '" href="javascript:void(0);" onclick="TrlHstManage.showPopup(\'' . $sUrl . '\');">' . _t('_trl_hst_admin_add_item', $this->_oDb->getModuleTitle($iId)) . '</a></div>';
    }

    function getItems($iId) {		
        $aRows = $this->_oDb->getItems($iId);
        if (!count($aRows))
            return _t('_trl_hst_no_items');

        $aItems = array();
        $aActionValues = array();
        $oForm = new BxTemplFormView(array());

        foreach ($aRows as $k => $v) {
            $aTable = array('type' => 'text', 'name' => 'unit', 'value' => $v['mat_alert_unit'], 'attrs' => array('style' => 'width:100px'), 'attrs_wrapper' => array('style' => 'width:100px;float:none;'));
            $aField = array('type' => 'text', 'name' => 'action', 'value' => $v['mat_alert_action'], 'attrs' => array('style' => 'width:100px'), 'attrs_wrapper' => array('style' => 'width:100px;float:none;'));


            $aItems[] = array(
                'title' => _t($v['mat_title']),
                'alert_unit' => $this->genWrapperInput($aTable, $oForm->genInput($aTable)),
                'alert_action' => $this->genWrapperInput($aField, $oForm->genInput($aField)),
                'checked' => $v['mat_active'] ? 'checked="checked"' : '',
                'delete_btn' => $this->getDeleteItemBtn($v['mat_id']),
                'id' => $v['mat_id']
            );
        }

        $aVars = array(
            'bx_repeat:module_actions' => $aItems
        );
		
		// echo '<pre>';
		// print_r($aVars);
		// echo '</pre>';
		
        return $this->parseHtmlByName('admin_actions', $aVars);
    }

    function  getDeleteItemPopup($id) {
        $aForm = array();
        $oForm = new BxTemplFormView($aForm);

        $aOkBtn = array(
            'type' => 'submit',
            'name' => 'hashtags_accept_delete',
            'value' => _t('_trl_hst_delete'),
            'attrs' => array(
                'onclick' => 'javascript:TrlHstManage.deleteItemYes(' . $id . '); return false;',
            )
        );
        $aCancelBtn = array(
            'type' => 'submit',
            'name' => 'hashtags_cancel_delete',
            'value' => _t('_trl_hst_cancel'),
            'attrs' => array(
                'onclick' => 'javascript:$(\'#hashtags_popup\').dolPopupHide();',
            )
        );
        $sOkBtn = $oForm->genInputButton($aOkBtn);
        $sCancelBtn = $oForm->genInputButton($aCancelBtn);

        $aForm = array(
            'form_attrs' => array(
                'action' => '',
                'onsubmit' => "javascript: return false;"
            ),
            'table_attrs' => array(
                'cellpadding' => 0,
                'cellspacing' => 0,
                'class' => 'buy-points-table'
            ),
            'inputs' => array(
                'msg' => array(
                    'type' => 'custom',
                    'content' => _t('_trl_hst_confirm_delete'),
                    'attrs' => array('id' => 'hashtags_title'),
                ),
                'buttons' => array(
                    'type' => 'custom',
                    'name' => 'energy_create_action',
                    'content' => $sOkBtn . $sCancelBtn,
                    'attrs' => array('id' => 'energy_create_action')
                ),
            )
        );

        $oForm = new BxTemplFormView($aForm);
        return PopupBox('hashtags_popup', _t('_trl_hst_confirm_delete_msg'), $oForm->getCode());
        ;
    }

    function genWrapperInput($aInput, $sContent) {
        $oForm = new BxTemplFormView(array());

        $sAttr = isset($aInput['attrs_wrapper']) && is_array($aInput['attrs_wrapper']) ? $oForm->convertArray2Attrs($aInput['attrs_wrapper']) : '';
        switch ($aInput['type']) {
            case 'textarea':
                $sCode = <<<BLAH
                        <div class="input_wrapper input_wrapper_{$aInput['type']}" $sAttr>
                            <div class="input_border">
                                $sContent
                            </div>
                            <div class="input_close_{$aInput['type']} left top"></div>
                            <div class="input_close_{$aInput['type']} left bottom"></div>
                            <div class="input_close_{$aInput['type']} right top"></div>
                            <div class="input_close_{$aInput['type']} right bottom"></div>
                        </div>
BLAH;
                break;

            default:
                $sCode = <<<BLAH
                        <div class="input_wrapper input_wrapper_{$aInput['type']}" $sAttr>
                            $sContent
                            <div class="input_close input_close_{$aInput['type']}"></div>
                        </div>
BLAH;
        }

        return $sCode;
    }

    function getDeleteItemBtn($iId) {
        return '<div align="right"><a id="add_new_action_' . $iId . '" href="javascript:void(0);" onclick="TrlHstManage.deleteItem(' . $iId . ');">' . _t('_trl_hst_admin_delete_item') . '</a></div>';
    }

    function getAddNewModuleForm() {
        if ($this->_oConfig == null)
            $this->_oConfig = BxDolModule::getInstance('TrlHstConfig');


        $sUrl = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'save_module';
        $sRedirect = BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'administration/manage';

        $aForm = array(
            'form_attrs' => array(
                'id' => 'hashtags-add-module',
                'name' => 'hashtags-add-module',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => '',
                'onsubmit' => "javascript:TrlHstManage.moduleExists(); return false;"
            ),
            'table_attrs' => array(
                'cellpadding' => 0,
                'cellspacing' => 0
            ),
            'inputs' => array(
                'uri' => array(
                    'type' => 'text',
                    'name' => 'hashtags_uri',
                    'value' => '',
                    'required' => true,
                    'caption' => _t('_trl_hst_admin_uri'),
                    'info' => _t('_trl_hst_admin_uri_info'),
                    'attrs' => array('id' => 'hashtags_uri'),
                ),
                'continue' => array(
                    'type' => 'submit',
                    'name' => 'hashtags_add_module',
                    'value' => _t('_trl_hst_continue'),
                    'attrs' => array(
                        'id' => 'hashtags_add_module',
                    )
                )
            ));

        $oForm = new BxTemplFormView($aForm);
        return PopupBox('hashtags_popup', _t('_trl_hst_admin_add_module'), $oForm->getCode());
    }

    function getModuleResult($sResult, $uri) {
        $sBtn = 'new';

        switch ($sResult) {
            case 'empty':
                $sMsg = _t('_trl_hst_add_mod_empty');
                break;

            case 'mod_not_exists':
                $sMsg = _t('_trl_hst_add_mod_not_exists', $uri);
                break;

            case 'mod_already_exits':
                $aModule = $this->_oDb->getModule($uri);
                $sMsg = _t('_trl_hst_add_mod_already_exists', $aModule['title'], $uri);
                break;

            case 'ok':
                $sBtn = 'save';
                $sTitle = $this->_oDb->getModuleTitle($uri);
                $sMsg = _t('_trl_hst_add_module_confirm', $sTitle);
                break;
        }

        if ($sBtn == 'new') {
            $aContinueBtn = array(
                'type' => 'submit',
                'name' => 'hashtags_add_module',
                'value' => _t('_trl_hst_back'),
                'attrs' => array(
                    'onclick' => 'javascript:TrlHstManage.getAddModuleForm(); return false;',
                )
            );
        }

        if ($sBtn == 'save') {

            $aContinueBtn = array(
                'type' => 'submit',
                'name' => 'hashtags_add_module',
                'value' => _t('_trl_hst_continue'),
                'attrs' => array(
                    'onclick' => 'javascript:TrlHstManage.saveModule(\'' . $uri . '\'); return false;',
                )
            );
        }



        $aCancelBtn = array(
            'type' => 'submit',
            'name' => 'kpi_cancel_add_module',
            'value' => _t('_trl_hst_cancel'),
            'attrs' => array(
                'onclick' => 'javascript:$(\'#hashtags_popup\').dolPopupHide();',
            )
        );

        $aForm = array();
        $oForm = new BxTemplFormView($aForm);

        $sContinueBtn = $oForm->genInputButton($aContinueBtn);
        $sCancelBtn = $oForm->genInputButton($aCancelBtn);


        $aForm = array(
            'form_attrs' => array(
                'action' => '',
                'onsubmit' => "javascript: return false;"
            ),
            'table_attrs' => array(
                'cellpadding' => 0,
                'cellspacing' => 0,
                'class' => 'buy-points-table'
            ),
            'inputs' => array(
                'msg' => array(
                    'type' => 'custom',
                    'content' => $sMsg,
                    'attrs' => array('id' => 'kpi_title'),
                ),
                'buttons' => array(
                    'type' => 'custom',
                    'name' => 'kpi_add_module',
                    'content' => $sContinueBtn . $sCancelBtn,
                    'attrs' => array('id' => 'kpi_add_module')
                ),
            )
        );

        $oForm = new BxTemplFormView($aForm);

        return PopupBox('hashtags_popup', _t('_trl_hst_admin_add_module'), $oForm->getCode());
    }
	
    function getReportModulePanelGrafics() {
		$dToday = date('Y-m-d H:i:00');
		$dPastDay = date('Y-m-d H:i:00', strtotime("-1 day"));
		$dPastWeek = date('Y-m-d H:i:00', strtotime("-1 week"));
		$dPast2Week = date('Y-m-d H:i:00', strtotime("-2 week"));
		$dPastMonth = date('Y-m-d H:i:00', strtotime("-1 month"));		
		
		$aPeriodsValid = array(
				0 => $dToday,
				1 => $dPastDay,
				2 => $dPastWeek,
				3 => $dPast2Week,
				4 => $dPastMonth,				
		);
		$sPeriodsValid = json_encode($aPeriodsValid);
		
		
		$aPeriodsTime =  array(0 => 'Custom' ,1 => 'Past Day', 2 => 'Past Week', 3 => 'Past 2 Week', 4 => 'Past Month');
        $aForm = array(
            'form_attrs' => array(
                'id' => 'hfa-add-item',
                'name' => 'hfa-add-item',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => '',
                'onsubmit' => "javascript: TrlHstManage.generateNps('false','" . _t("_trl_hst_graphic_title_promoter_detractor") . "',true,true);  $('#hfa-button-nps').addClass('active-hfa');return false;"
            ),
            'params' => array(
                'db' => array(
                    'table' => '',
                    'key' => 'id',
                    'submit_name' => 'submit_form',
                ),
            ),
            'inputs' => array(
				'hashtag' => array(
					'type' => 'text',
					'name' => 'hashtags_to_graphic[]',
					'caption' => _t('_trl_hst_hasgtag_name'),
					'required' => true,
					'value' =>$_POST['hashtag_search'],
					'attrs' => array(
						'multiplyable' => 'true'
					),
					'checker' => array (  
                         'func' => 'length',
                         'params' => array(3,100),
                         'error' => 'length must be from 3 to 100 characters',
                    ),
				),
				'period' => array(
					'type' => 'select',
					'name' => 'hashtag_period',
					'caption' => _t('_trl_hst_period_time'),
					'values' => $aPeriodsTime,
					'value' => $_POST['hashtag_period'],
					'required' => true,
					'attrs' => array('onchange' => "javascript:TrlHstManage.changeDates(".$sPeriodsValid.");"),					
					'checker' => array(
						'error' => _t('_me_blgg_form_err_title'),
					),
				),	
			
			
                'from' => array(
                    'type' => 'datetime',
                    'name' => 'hfa_date_from',
                    'caption' => _t('_trl_hfa_admin_report_from'),
                    'required' => true,
					'value' => $dToday,
                    'attrs' => array('id' => 'hfa_date_from'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                ),
                'to' => array(
                    'type' => 'datetime',
                    'name' => 'hfa_date_to',
                    'caption' => _t('_trl_hfa_admin_report_to'),
                    'required' => true,
					'value' => $dToday,
                    'attrs' => array('id' => 'hfa_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                ),
                'submit' => array(
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_trl_hst_compare'),
                    'colspan' => true                    
                ),
            ),
        );


        $oForm = new BxTemplFormView($aForm);
       
		$sContent.=
			'<div >'.$this->getDescriptionModule().
				'<div >
					'.$oForm->getCode().'
				</div>        
			</div>
			</div>
			<div class="clear">
			</div>';


        $sContent.=$this->getHTMLGraphic();

        return $sContent; // output posts list
    }
	
	function getHTMLGraphic(){
		$this->addAdminCss('admin.css');
		$this->addAdminJs('highcharts.js');
		$aVars = array(
			'bx_repeat:module_actions' => $aItems,
			'header_export' => BX_DOL_URL_ROOT . $this->_oConfig->getBaseUri() . 'get_header_export/'
		);
		return $this->parseHtmlByName('admin_report_graphics', $aVars);		
	}
	
	function getDescriptionModule(){
        $sHtml='<div class="span4" style="heigth:50px">
                    <h3></h3>
                    <p style="font-size:12px"> '._t('_trl_hst_description_module').'</b></p>                   
                  </div>
                  "';

        return $sHtml;
    }
	
	function getFormReportDataOfHst(){
		$dToday = date('Y-m-d H:i:00');
		$dPastDay = date('Y-m-d H:i:00', strtotime("-1 day"));
		$dPastWeek = date('Y-m-d H:i:00', strtotime("-1 week"));
		$dPast2Week = date('Y-m-d H:i:00', strtotime("-2 week"));
		$dPastMonth = date('Y-m-d H:i:00', strtotime("-1 month"));		
		
		$aPeriodsValid = array(
				0 => $dToday,
				1 => $dPastDay,
				2 => $dPastWeek,
				3 => $dPast2Week,
				4 => $dPastMonth,				
		);
		$sPeriodsValid = json_encode($aPeriodsValid);
		
		
		$aPeriodsTime =  array(0 => 'Custom' ,1 => 'Past Day', 2 => 'Past Week', 3 => 'Past 2 Week', 4 => 'Past Month');
		
		 $aForm = array(
            'form_attrs' => array(
                'id' => 'hfa-add-item',
                'name' => 'hfa-add-item',
                'method' => 'post',
                'enctype' => 'multipart/form-data',
                'action' => ''
            ),
            'params' => array(
                'db' => array(
                    'table' => '',
                    'key' => 'id',
                    'submit_name' => 'submit_form',
                ),
            ),
            'inputs' => array(
				'hashtag' => array(
					'type' => 'text',
					'name' => 'hashtag_search',
					'caption' => _t('_trl_hst_hasgtag_name'),
					'required' => true,
					'value' =>$_POST['hashtag_search'],					
					'checker' => array(
						'error' => _t('_me_blgg_form_err_title'),
					),
				),		

				'period' => array(
					'type' => 'select',
					'name' => 'hashtag_period',
					'caption' => _t('_trl_hst_period_time'),
					'values' => $aPeriodsTime,
					'value' => $_POST['hashtag_period'],
					'required' => true,
					'attrs' => array('onchange' => "javascript:TrlHstManage.changeDates(".$sPeriodsValid.");"),						
					'checker' => array(
						'error' => _t('_me_blgg_form_err_title'),
					),
				),	
				
                'from' => array(
                    'type' => 'datetime',
                    'name' => 'hfa_date_from',
                    'caption' => _t('_trl_hfa_admin_report_from'),
					'value' => $_POST['hfa_date_from'],
                    'required' => true,
                    'attrs' => array('id' => 'hfa_date_from'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                ),
                'to' => array(
                    'type' => 'datetime',
                    'name' => 'hfa_date_to',
                    'caption' => _t('_trl_hfa_admin_report_to'),
					'value' => $_POST['hfa_date_to'],
                    'required' => true,
                    'attrs' => array('id' => 'hfa_date_to'),
                    'checker' => array(
                        'error' => _t('_me_blgg_form_err_title'),
                    ),
                ),
                'submit' => array(
                    'type' => 'submit',
                    'name' => 'submit_form',
                    'value' => _t('_trl_hfa_admin_report_search'),
                    'colspan' => true,
                    
                ),
            ),
        );
        $oForm = new BxTemplFormView($aForm);       
		$sContent.= $oForm->getCode();      
		
        echo $sContent;				
		if(isset($_POST['submit_form'])){
			$sHstSearch =$_POST['hashtag_search'];
			$iHstPeriod =$_POST['hashtag_period'];
			$dHstFrom =$_POST['hfa_date_from'];
			$dHstTo =$_POST['hfa_date_to'];
			
			$aInfoReportRows = $this->_oDb->getHashtagsReportTable($sHstSearch, $dHstFrom,$dHstTo );
			$i=0;
			for($i=0; $i < count($aInfoReportRows); $i++){				
				$aInfoReportRows[$i]['hashtag_link'] = BX_DOL_URL_ROOT.'m/hashtags/view?hst='.str_replace('#','',$aInfoReportRows[$i]['hashtag']);
				$aInfoReportRows[$i]['total_likes'] = 0;								
			}
			
			$aInfoTableReport = array(
			'url_plugin_datatable' => BX_DOL_URL_ROOT.'modules/transactel/hashtags/templates/base/datatable',
			'bx_repeat:hashtags_info_rows' =>$aInfoReportRows
			);			
			
			echo $this->parseHtmlByName('report_hahstags', $aInfoTableReport);
		}		
	}
}

?>