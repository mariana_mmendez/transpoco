<?php
bx_import('BxDolPageView');

class TrlHstPageHashtagsView extends BxDolPageView
{
    var $_oMain;

    function TrlHstPageHashtagsView(&$oMain)
    {
        $this->_oMain = $oMain;
        parent::BxDolPageView('trl_hashtags_view');
    }

    function getBlockCode_Categories()
    {
        return $this->_oMain->genBlockCategories();
    }

    function getBlockCode_FeaturedDocsHome()
    {
        return $this->_oMain->genBlockFeaturedDocsHome();
    }
    
    function getBlockCode_LatestDocsHome()
    {
    	return $this->_oMain->genBlockLatestDocsHome();
    }

}




?>