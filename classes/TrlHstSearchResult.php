<?php
bx_import('BxDolTwigSearchResult');

class TrlHstSearchResult extends BxDolTwigSearchResult {

 var $aCurrent = array(
        'name' => 'trl_hashtags',
        'title' => '_trl_hashtags',
        'table' => 'trl_hst_hashtags',
        'ownFields' => array('hst_id', 'hst_name', 'hst_owner_id', 'hst_timestamp'),
        'searchFields' => array('hst_name', 'hst_owner_id', 'hst_timestamp')
    );
}




?>