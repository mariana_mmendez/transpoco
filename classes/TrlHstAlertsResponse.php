<?

bx_import('BxDolAlerts');
bx_import('BxDolModule');

class TrlHstAlertsResponse extends BxDolAlertsResponse {

    var $_oMain;

    function TrlHstAlertsResponse() {
        parent::BxDolAlertsResponse();
        $this->_oMain = BxDolModule::getInstance('TrlHstModule');
    }

    function response($oAlert) {		
        $infouser = getProfileInfo($oAlert->iSender);
        $aAlert = array(
            'sUnit' => $oAlert->sUnit,
            'sAction' => $oAlert->sAction,
            'iObject' => $oAlert->iObject,
            'iowner' => $infouser['ID'],
            'iownerposition' => $infouser['Position_id'],
            'iownerunit' => $infouser['Unit_id'],
            'iownersuperunit' => $infouser['SuperUnit_id'],
            'aExtras' => $oAlert->aExtras
        ); 
        $this->_oMain->setAlert($aAlert);
        $this->_oMain->setNotification();
    }
}
?>