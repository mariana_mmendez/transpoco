-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_tlife_sv.trl_hst_hashtags
CREATE TABLE IF NOT EXISTS `trl_hst_hashtags` (
  `hst_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique auto increment ID',
  `hst_name` varchar(250) CHARACTER SET latin1 NOT NULL COMMENT 'Hashtag name ',
  `hst_owner_id` int(11) NOT NULL COMMENT 'ID of the first user who use the hashtag',
  `hst_timestamp` int(11) DEFAULT NULL COMMENT 'Timestamp of the first time when the hashtag was used',
  PRIMARY KEY (`hst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Hashtags registered on the site';

-- Data exporting was unselected.


-- Dumping structure for table db_tlife_sv.trl_hst_matrix
CREATE TABLE IF NOT EXISTS `trl_hst_matrix` (
  `mat_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique auto increment ID',
  `mat_title` varchar(200) CHARACTER SET latin1 NOT NULL COMMENT 'Alert handler title description',
  `mat_mod_id` int(11) NOT NULL COMMENT 'Module ID of  the handler',
  `mat_alert_unit` varchar(100) CHARACTER SET latin1 NOT NULL COMMENT 'Unit of the handler ',
  `mat_alert_action` varchar(100) CHARACTER SET latin1 NOT NULL COMMENT 'Action of the handler',
  `mat_active` tinyint(1) NOT NULL COMMENT 'If the handler is active ',
  `mat_name_table` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `mat_field_table` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`mat_id`),
  KEY `mat_mod_id` (`mat_mod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Register the alerts per module';

-- Data exporting was unselected.


-- Dumping structure for table db_tlife_sv.trl_hst_modules
CREATE TABLE IF NOT EXISTS `trl_hst_modules` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique auto increment ID',
  `mod_uri` varchar(250) CHARACTER SET latin1 NOT NULL COMMENT 'Module uri',
  `mod_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Module active on hashtags',
  `mod_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`mod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='modules used on the module of hashtags';

-- Data exporting was unselected.


-- Dumping structure for table db_tlife_sv.trl_hst_objects
CREATE TABLE IF NOT EXISTS `trl_hst_objects` (
  `obj_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique auto increment ID',
  `obj_event_type` varchar(200) CHARACTER SET latin1 NOT NULL COMMENT 'Type of event ',
  `obj_object_id` int(11) NOT NULL COMMENT 'ID of the object that contains the hashtag',
  `obj_hst_id` int(11) NOT NULL COMMENT 'ID of the hashtag',
  `obj_owner_id` int(11) NOT NULL COMMENT 'ID of the owner of the object',
  `obj_timestamp` int(11) NOT NULL COMMENT 'Timestamp of the object when the hashtag have been used',
  PRIMARY KEY (`obj_id`),
  KEY `obj_hst_id` (`obj_hst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keep the track of any object wich contains hashtags';

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


INSERT INTO `sys_menu_top` (`ID`, `Parent`, `Name`, `Caption`, `Link`, `Order`, `Visible`, `Target`, `Onclick`, `Check`, `Movable`, `Clonable`, `Editable`, `Deletable`, `Active`, `Type`, `Picture`, `Icon`, `BQuickLink`, `Statistics`) 
VALUES (null, 0, 'Hashtags', '_trl_hst_hashtags', 'm/hashtags/home|m/hashtags|modules/?r=hashtags', 16, 'non,memb', '', '', '', 3, 1, 1, 1, 1, 'top', '', '', 0, '');

INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`, `Cache`) VALUES (null, 'trl_hashtags_home', '998px', 'trl_hst_trending_topics', 'trl_hst_trending_topics', 1, 0, 'PHP', 'return BxDolService::call(\'hashtags\', \'get_home_topics\',array(35));', 1, 25.2, 'non,memb', 0, 0);
INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`, `Cache`) VALUES (null, 'trl_hashtags_home', '998px', 'trl_hashtags_events', 'trl_hashtags_events', 2, 0, 'PHP', 'return BxDolService::call(\'hashtags\', \'get_home_main\');', 1, 74.8, 'non,memb', 0, 0);
INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`, `Cache`) VALUES (null, 'trl_hashtags_view', '998px', 'trl_hst_trending_topics', 'trl_hst_trending_topics', 1, 0, 'PHP', 'return BxDolService::call(\'hashtags\', \'get_home_topics\',array(50));', 1, 25.2, 'non,memb', 0, 0);
INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`, `Cache`) VALUES (null, 'trl_hashtags_view', '998px', 'trl_hashtags_events', 'trl_hashtags_events', 2, 0, 'PHP', 'return BxDolService::call(\'hashtags\', \'get_view_main\');', 1, 74.8, 'non,memb', 0, 0);
INSERT INTO `sys_page_compose` (`ID`, `Page`, `PageWidth`, `Desc`, `Caption`, `Column`, `Order`, `Func`, `Content`, `DesignBox`, `ColWidth`, `Visible`, `MinWidth`, `Cache`) VALUES (null, 'index', '1100', 'trl_hst_trending_topics', 'trl_hst_trending_topics', 1, 1, 'PHP', 'return BxDolService::call(\'hashtags\', \'get_home_topics\',array(10));', 1, 22.1, 'non,memb', 0, 0);


