<?

$aConfig = array(
    /**
     * Main Section.
     */
    'title' => 'Hashtags module',
    'version' => '1.0.0',
    'vendor' => 'Transactel',
    'update_url' => '',
    'compatible_with' => array(
        '7.x.x'
    ),
    /**
     * 'home_dir' and 'home_uri' - should be unique. Don't use spaces in 'home_uri' and the other special chars.
     */
    'home_dir' => 'transactel/hashtags/',
    'home_uri' => 'hashtags',
    'db_prefix' => 'trl_hst_',
    'class_prefix' => 'TrlHst',
    /**
     * Installation/Uninstallation Section.
     */
    'install' => array(
        'show_introduction' => 1,
        'execute_sql' => 1,
        'update_languages' => 1,
        'recompile_main_menu' => 1,
        'recompile_permalinks' => 1,
        'show_conclusion' => 0
    ),
    'uninstall' => array(
        'execute_sql' => 1,
        'update_languages' => 1,
        'recompile_main_menu' => 1,
        'recompile_permalinks' => 1,
        'show_conclusion' => 0
    ),
    /**
     * Dependencies Section
     */
    'dependencies' => array(),
    /**
     * Category for language keys.
     */
    'language_category' => 'Hashtags',
    /**
     * Permissions Section
     */
    'install_permissions' => array(),
    'uninstall_permissions' => array(),
    /**
     * Introduction and Conclusion Section.
     */
    'install_info' => array(
        'introduction' => 'inst_intro.html',
        'conclusion' => 'inst_concl.html'
    ),
    'uninstall_info' => array(
        'introduction' => 'uninst_intro.html',
        'conclusion' => 'uninst_concl.html'
    )
);
?>